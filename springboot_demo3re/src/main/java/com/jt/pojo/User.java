package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data//动态生成set/get方法
@Accessors(chain = true)
@TableName("demo_user")//实现对象和表名的映射
public class User {
    //设定主键自增
    @TableId(type = IdType.AUTO)
    private Integer id;
    //@TableField("name")//实现属性与字段映射，规则: 如果属性与字段的名称一致,则注解可以省略
    private String name;
    //@TableField("age")
    private Integer age;
    //@TableField("sex")
    private String sex;

}
