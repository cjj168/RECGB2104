package com.jt.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data//动态生成set/get方法
@Accessors(chain = true)//链式加载,重写了set()
public class User {
    private Integer id;
    private String name;
    private Integer age;
    private String sex;
}
