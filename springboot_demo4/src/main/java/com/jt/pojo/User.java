package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@TableName("demo_user")
@Accessors(chain = true)
//规定：POJO实体一般都需要实现序列接口
public class User implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id ;
    private String name ;
    private Integer age ;
    private String sex ;
}
