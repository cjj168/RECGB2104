package com.jt.mapper;

import com.jt.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper
public interface UserMapper {
    List<User> findAll();

    //注意事项：映射文件和注解二选一
    @Select("select * from demo_user where id = #{id}")
    //@Insert("") //新增时使用 “更新”
    //@Update("") //更新
    //@Delete("") //删除
    User findUserById(int id);

    @Insert("insert into demo_user(id,name,age,sex) value(null,#{name},#{age},#{sex})")
    void insert(User user);

    @Update("update demo_user set age = #{age},sex = #{sex} where name = #{name}")
    void updateByName(User user);

    @Delete("delete from demo_user where id = #{id}")
    void deleteById(int id);
}
