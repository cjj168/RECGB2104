package com.jt;

import com.jt.controller.UserController;
import com.jt.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringbootDemo1Tests {

//    @Autowired
//    private UserService userService;
    @Autowired
    private UserController userController;

    @Test
    void test01(){
//        String msg = userService.getMsg();
//        System.out.println(msg);
        String msg = userController.getMsg();
        System.out.println(msg);
    }
}
