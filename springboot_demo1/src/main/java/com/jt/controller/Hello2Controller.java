package com.jt.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@PropertySource(value = "classpath:/user.properties",encoding ="utf-8")
public class Hello2Controller {
    //private String name = "小张三";
    @Value("${userinfo.name}")
    private String name ;
    @Value("${user.info2}")
    private String name2 ;
    @RequestMapping("hello2")
    public String hello(){
        return "您好SpringBoot:"+name+name2;
    }
}
