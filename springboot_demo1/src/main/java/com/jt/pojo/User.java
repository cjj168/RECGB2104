package com.jt.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)//开启链式加载
public class User {
    private Integer id;
    private String name;
    private Integer age;
    private String sex;

//    //this 代表当前对象
//    public User setId(Integer id){
//        this.id = id ;
//        return this;
//    }
//
//    public User setName(String name){
//        this.name = name ;
//        return this ;
//    }
}
