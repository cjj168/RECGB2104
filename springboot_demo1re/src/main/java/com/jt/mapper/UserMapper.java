package com.jt.mapper;

import com.jt.pojo.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {
    User get();
}
