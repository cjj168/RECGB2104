package com.jt.test;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StringUtils;

import java.util.List;

@SpringBootTest
public class TestMybatis {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void findAll() {
        System.out.println(userMapper.findAll());
    }

    @Test
    public void testInsert() {
        User user = new User();
        user.setName("MP").setAge(25).setSex("大哥");
        userMapper.insert(user);
        System.out.println("新增成功");
    }

    //1.根据id主键，查询id=1的数据
    //SELECT id,name,age,sex FROM demo_user WHERE id=?
    @Test
    public void testSelect() {
        User user = userMapper.selectById(1);
        System.out.println(user);
    }

    //Sql: select * from demo_user where name="xx" and sex="xx"
    //2.查询name="小桥" sex="男"的用户
    @Test
    public void testSelect2() {
        //创建条件构造器 封装where条件的.
        User user = new User();
        user.setName("小乔").setSex("男");
        //实现时会动态的根据对象中不为null的属性,拼接where条件,
        //默认的关系链接符 and
        QueryWrapper queryWrapper = new QueryWrapper(user);
        List<User> userList = userMapper.selectList(queryWrapper);
        System.out.println(userList);
    }

    //3.查询name="小乔" sex="男"的用户
    //Sql:SELECT id,name,age,sex FROM demo_user WHERE name=? AND sex=?
    @Test
    public void testSelect3() {
        QueryWrapper<User> queryWrapper = new QueryWrapper();
        queryWrapper.eq("name", "小桥")
                .eq("sex", "男");
        List<User> userList = userMapper.selectList(queryWrapper);
        System.out.println(userList);
    }

    //查询 age>18 sex="女"的用户
    //Sql:select * demo_user where age >18 and sex ="女"
    //逻辑运算符 > gt, < lt, = eq, >= ge, <= le, != ne
    @Test
    public void testSelect4() {
        QueryWrapper<User> queryWrapper = new QueryWrapper();
        queryWrapper.gt("age", 18)
                .eq("sex", "女");
        List<User> userList = userMapper.selectList(queryWrapper);
        System.out.println(userList);
    }

    @Test
    public void testSelect5() {
        QueryWrapper<User> queryWrapper = new QueryWrapper();
        //queryWrapper.like("name", "乔");
        queryWrapper.likeLeft("name", "乔");
        List<User> userList = userMapper.selectList(queryWrapper);
        System.out.println(userList);
    }


    /**
     * 6.in 关键字
     * 需求：查询ID为1,3,5,6的数据
     * Sql:select * from demo_user where id in(1,3,5,6)
     * java基础：参数中使用...b表示可变参数类型 duo
     */
    @Test
    public void testSelect6() {
        //一般的数组采用包装类型,使用对象身上的方法  基本类型没有方法
        Integer[] ids = {1, 3, 5, 6};
        QueryWrapper<User> queryWrapper = new QueryWrapper();
        queryWrapper.in("id", ids);
        //采用可变参数类型 实现查询
        //queryWrapper.in("id", 1,3,4,5,6);
        List<User> userList = userMapper.selectList(queryWrapper);
        System.out.println(userList);
    }

    /**关键字：order by 排序
     * 默认规则：升序asc  如果降序desc
     * 需求：查询性别为男的用户并按照年龄降序排列
     * Sql：select * from demo_user where sex="男" order by age desc*/
    @Test
    public void testSelect7() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sex", "男")
                    .orderByDesc("age");
        List<User> userList = userMapper.selectList(queryWrapper);
        System.out.println(userList);

    }
    /**
     * 动态sql:
     * 根据用户的条件，动态的拼接where条件
     * 案例：根据sex,age查询数据
     * 1.select * from demo_user where age > 18 and sex="女"
     * API说明：
     * queryWrapper.gt(判断条件,字段名称,字段值)
     *            判断条件: true  则动态的拼接where条件
     *                    false 不会拼接where条件
     * 判断语句:
     *        Boolean sexBoo = (sex !=null) && sex.length()>0;
     */
    @Test
    public void testSelect8() {
        Integer age =18;
        String sex = "女";
        QueryWrapper<User> queryWrapper = new QueryWrapper();
        Boolean ageBoo = (age != null);
        Boolean sexBoo = StringUtils.hasLength(sex);
        queryWrapper.gt(ageBoo, "age", age)
                    .eq(sexBoo, "sex", "女");
        List<User> userList = userMapper.selectList(queryWrapper);
        System.out.println(userList);
    }

    @Test
    public void testUpdate(){
        User user = new User();
        user.setId(232).setName("晚上吃什么");
        userMapper.updateById(user);
    }
}