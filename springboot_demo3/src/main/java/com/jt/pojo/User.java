package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data//动态生成set/get方法
@Accessors(chain = true)//链式加载,重写了set()
@TableName("demo_user")//实现对象与表名映射
public class User {
    @TableId(type = IdType.AUTO)
    private Integer id;
    //@TableField("name")
    private String name;
    private Integer age;
    private String sex;
}
