package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import javafx.print.PageLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/findAll")
    public List<User> findAll(){

        return userService.findAll();
    }

    /**
     * 根据ID删除数据
     * URL:http://localhost:8090/user/100
     * 参数:id
     * 返回值: String类型
     */
    @DeleteMapping("/user/{id}")
    public String deleteUserById(@PathVariable Integer id){
        userService.deleteUserById(id);
        return "删除数据"+id+"成功!!!";

    }

    /**
     * 需求: 完成用户的入库操作
     * URL: http://localhost:8090/saveUser
     * 参数: 对象的JSON串. {key:value}
     * 类型: POST
     * 返回值: String
     * 难点: 如何将JSON串转化为User对象!
     * SpringMVC: 针对于JSON与对象转化 开发2个注解
     *            1.@ResponseBody 将对象转化为JSON串
     *            2.@RequestBody  将JSON串转化为User对象
     */
    @PostMapping("/saveUser")
    public String saveUser(@RequestBody User user){

        userService.saveUser(user);
        return "新增用户成功";
    }

    /**
     * 实现用户列表修改操作
     * URl:http://localhost:8090/updateUser
     * 参数：js对象 经过http协议转为 JSON串。{key:value}
     * 类型：put
     * 返回值：String
     * 难点：如何将JSON串转化为User对象！
     *       SpringMVC:针对于JSON与对象转换 开发了两个注解
     *              1.@ResponseBody 将对象转化为JSON串
     *              2.@RequestBody 将JSON串转化为对象
     */
    @PutMapping("/updateUser")
    public String updateUser(@RequestBody User user){

        userService.updateUser(user);
        return "修改数据成功";
    }

}
