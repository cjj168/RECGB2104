package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
//@Mapper
public interface UserMapper  {
    List<User> findAll();

    void insert(String name, int age, String sex);

    User selectById(int id);

    User select2(String name, String sex);

    List<User> select3(int age, String sex);

    List<User> select4(String  name);

    List<User> select5(String name);

    List<User> select6(int id1, int id2, int id3, int id4);

    List<User> select7(String sex);

    List<User> select8(int i, String sex);


    List<User> select9();

    void deleteById(int id);

    void deleteByName(String name);

    void updateById(String name, int id);

    void update(String name, int age, String sex, int id);
}
