package com.jt.test;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class TestMybatis {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void testFind() {
        System.out.println(userMapper.getClass());
        List<User> userList = userMapper.findAll();
        System.out.println(userList);
    }

    //INSERT INTO demo_user ( name, age, sex ) VALUES ( ?, ?, ? )
    @Test
    public void testInsert() {
        userMapper.insert("李白",50,"男");
        System.out.println("新增成功");
    }

    //1.根据id主键 查询id=1的数据
    //SELECT id,name,age,sex FROM demo_user WHERE id=?
    @Test
    public void testSelect1() {
        User user = userMapper.selectById(1);
        System.out.println(user);
    }


    //2.查询name="小乔" sex="男"的用户
    //Sql:select * from demo_user where name="?" and sex="?"
    @Test
    public void testSelect2() {
        User user = userMapper.select2("小乔","男");
        System.out.println(user);
    }

    //4.查询 age> 18 sex="女"的用户
    //select * from demo_user where age > 18 and sex ="女"
    //逻辑运算符  > gt, < lt, = eq, >= ge, <= le, != ne
    @Test
    public void testSelect3() {
        List<User> userList = userMapper.select3(18,"女");
        System.out.println(userList);
    }

    //5. like关键字
    //5.1 查询name包含"乔" where name like '%乔%'
    //5.2 查询name以乔结尾的 where name like '%乔'
    @Test
    public void testSelect4() {
        //List<User> userList = userMapper.select4("%乔%");
        List<User> userList2 = userMapper.select5("%乔");
        //System.out.println(userList);
        System.out.println(userList2);
    }

    /**
     * 6. in 关键字
     * 需求: 查询ID为1,3,5,6的数据
     * Sql: select * from demo_user where id in (1,3,5,6)
     */
    @Test
    public void testSelect6() {
        List<User> userList = userMapper.select6(1,3,5,6);
        System.out.println(userList);
    }

    /**
     * 关键字: order by  排序
     * 默认规则:  升序 asc   如果降序 desc
     * 需求: 查询性别为男的用户并且 按照年龄降序排列.
     * Sql: select * from demo_user where sex="男" order by age desc
     */
    @Test
    public void testSelect7() {
        List<User> userList = userMapper.select7("男");
        System.out.println(userList);
    }

    /**
     * 动态Sql:
     * 根据用户的条件,动态的拼接where条件
     * 案例: 根据sex,age查询数据
     * 1.select * from demo_user where age > 18 and sex="女"
     */
    @Test
    public void testSelect8() {
        List<User> userList = userMapper.select8(18,"女");
        System.out.println(userList);
    }

    /**
     * 练习9: 只获取主键ID的值
     * Sql: select id from demo_user
     */
    @Test
    public void testSelect9(){
        List<User> userList= userMapper.select9();
        System.out.println(userList);
    }

    /**
     * 练习10: 删除name="xxx"的数据
     */
    @Test
    public void testDelete() {
        //删除ID为100的数据
        userMapper.deleteById(100);
        System.out.println("删除成功");
        userMapper.deleteByName("xxx");
        System.out.println("删除成功");
    }

    /**
     * 练习11: 数据修改
     * 案例1:  要求修改id=233 name改为="晚上吃什么"
     */
    @Test
    public void testUpdate() {
        //修改除ID之外的所有不为null的数据,id当作where一位条件
        userMapper.updateById("晚上吃什么",233);
        System.out.println("修改成功");
    }

    /**
     * 练习12: 数据修改
     * 案例2:  将name=mp的用户改为name="宵夜吃什么" age=20 sex=女
     */
    @Test
    public void testUpdate2() {
        userMapper.update("宵夜吃什么",20,"女",236);
        System.out.println("修改成功");
    }
}


