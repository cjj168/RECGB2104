package com.jt.service;

import com.jt.pojo.ItemCat;
import com.jt.pojo.StrType;

import java.util.List;

public interface StrTypeService {


  
    

    List<StrType> findStrTypeList(Integer level);

    void updateStatus(StrType strType);

    void saveStrType(StrType strType);
}
