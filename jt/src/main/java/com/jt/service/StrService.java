package com.jt.service;

import com.jt.pojo.Str;
import com.jt.vo.PageResult;

import java.util.List;


public interface StrService {

    PageResult getItemList(PageResult pageResult);

    void updateStrStatus(Str str);

    void deleteStrById(Integer id);
}
