package com.jt.service;

import com.jt.mapper.StrTypeMapper;
import com.jt.pojo.StrType;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StrTypeServiceImpl implements StrTypeService{

    @Autowired
    private StrTypeMapper strTypeMapper;



    /**
     * 1.准备Map集合,实现数据封装
     * Map<key,value> = Map<parentId,List<StrType>>
     * 2.封装业务说明
     *      map中key~~parentId
     *         不存在:可以存储该key
     *               同时封装一个List集合,将自己作为第一个元素封装到其中.
     *         存在: 根据key获取所有子级集合,将自己追加进去 形成第二个元素.
     */

    public Map<Integer,List<StrType>> StrTypeMap(){
        //1.定义Map集合
        Map<Integer,List<StrType>> map = new HashMap<>();
        //2.查询所有的数据库信息 1-2-3
        List<StrType> list = strTypeMapper.selectList(null);
        for (StrType strType : list){
            Integer parentId = strType.getParentId();//获取父级ID
            if (map.containsKey(parentId)){//判断集合中是否已经有parentId
                //有key  获取list集合 将自己追加到集合中
                List<StrType> exitList = map.get(parentId);//引用对象
                exitList.add(strType);
            }else {
                //没有key,将自己封装为第一个list元素
                List<StrType> firstList = new ArrayList<>();
                firstList.add(strType);
                map.put(parentId,firstList);
            }
        }
        return map;
    }





    @Override
    public List<StrType> findStrTypeList(Integer level) {

        Map<Integer, List<StrType>> map = StrTypeMap();
        //1.如果level=1 说明获取一级游记分类信息 parent_id=0
        if (level == 1){
            return map.get(0);
        }
        if (level == 2){//获取一级和二级菜单信息
         return getTowList(map);
        }
        //3.获取三级菜单信息
        //3.1获取二级游记分类信息  BUG:有的数据可能没有子级 如何处理
        List<StrType> oneList = getTowList(map);
        for (StrType oneStrType :oneList) {
            //从一级集合中,获取二级菜单列表
            List<StrType> towList = oneStrType.getChildren();
            //bug解决: 如果该元素没有2级列表,则跳过本次循环,执行下一次操作
            if (towList == null || towList.size() == 0){
                continue;
            }
            for (StrType towStrType:towList){
                //查询三级游记分类  条件:parentId=2级ID
                List<StrType> threeList = map.get(towStrType.getId());
                towStrType.setChildren(threeList);
            }
        }
        return oneList;
    }

    public List<StrType> getTowList(Map<Integer, List<StrType>> map) {
        List<StrType> oneList = map.get(0);
        for (StrType oneStrType:oneList){//查询二级 parentId=1级Id
            List<StrType> towList = map.get(oneStrType.getId());
            oneStrType.setChildren(towList);
        }
        //二级嵌套在一级集合中,所有永远返回的都是1级.
        return oneList;
    }

    @Override
    public void updateStatus(StrType strType) {
        //根据不为null的属性充当set条件,id充当where条件
        strTypeMapper.updateById(strType);
    }

    @Override
    @Transactional
    public void saveStrType(StrType strType) {
        strType.setStatus(true);
        strTypeMapper.insert(strType);
    }


}