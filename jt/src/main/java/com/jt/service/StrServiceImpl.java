package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.StrMapper;
import com.jt.pojo.Item;
import com.jt.pojo.Str;
import com.jt.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class StrServiceImpl implements StrService{
    @Autowired
    private StrMapper strMapper;


    @Override
    public PageResult getItemList(PageResult pageResult) {

        IPage page = new Page(pageResult.getPageNum(),pageResult.getPageSize());
        QueryWrapper queryWrapper = new QueryWrapper();
        boolean flag = StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag,"title",pageResult.getQuery());
        //执行分页查询,返回值依然是分页对象信息
        page = strMapper.selectPage(page,queryWrapper);
        //回传5个参数 total/分页后的数据
        long total = page.getTotal();
        List<Item> rows = page.getRecords();
        return pageResult.setTotal(total).setRows(rows);
    }

    @Override
    public void updateStrStatus(Str str) {
        strMapper.updateById(str);
    }

    @Override
    public void deleteStrById(Integer id) {
        strMapper.deleteById(id);
    }
}
