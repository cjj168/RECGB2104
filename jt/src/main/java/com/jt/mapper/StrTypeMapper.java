package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.StrType;

public interface StrTypeMapper extends BaseMapper<StrType> {
}
