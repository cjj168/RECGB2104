package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.User;
import com.jt.vo.PageResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper extends BaseMapper<User> {

//    List<User> getUserList(int startIndex, int pageSize);

    //方式2: 利用对象进行查询
//    List<User> getUserList2(PageResult pageResult);
}
