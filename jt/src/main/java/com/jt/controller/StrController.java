package com.jt.controller;

import com.jt.pojo.Item;
import com.jt.pojo.Str;
import com.jt.service.StrService;
import com.jt.vo.PageResult;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/str")
public class StrController {
    @Autowired
    private StrService strService;


    /**
     * URL地址:/str/getItemList?query=&pageNum=1&pageSize=10
     * 参数: pageResult对象
     * 返回值: SysResult(pageResult)
     */
    @GetMapping("/getStrList")
    public SysResult getItemList(PageResult pageResult) {

        pageResult = strService.getItemList(pageResult);
        return SysResult.success(pageResult);
    }

    /**
     * 游记状态修改
     * URL: /str/updateStrStatus
     * 参数: form表单对象
     * 返回值: SysResult
     */
    @PutMapping("/updateStrStatus")
    public SysResult updateStrStatus(@RequestBody Str str){
        strService.updateStrStatus(str);
        return SysResult.success();
    }
    /**
     * 游记删除操作
     * URL:  /str/deleteStrById
     * 参数: id
     * 返回值: SysResult对象
     */
    @DeleteMapping("/deleteStrById")
    public SysResult deleteStrById(Integer id){
        strService.deleteStrById(id);
        return SysResult.success();
    }
    /**
     * 游记新增操作
     * URL地址:  http://localhost:8091/str/saveStr
     * 请求参数:  form表单的提交对象~~~JSON串 StrVO对象接收
     * 返回值:   SysResult对象
     */























}