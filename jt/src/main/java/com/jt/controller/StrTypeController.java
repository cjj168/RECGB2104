package com.jt.controller;


import com.jt.pojo.ItemCat;
import com.jt.pojo.StrType;
import com.jt.service.StrTypeService;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/strType")
@CrossOrigin
public class StrTypeController {

    @Autowired
    private StrTypeService strTypeService;
    /**
     *  URL: /strType/findStrTypeList/{level}
     *  参数: level 1/2/3
     *  返回值: SysResult对象(List<StrType>)
     */
    @GetMapping("/findStrTypeList/{level}")
    public SysResult findItemCatList(@PathVariable Integer level){

        List<StrType> strTypeList = strTypeService.findStrTypeList(level);
        return SysResult.success(strTypeList);
    }

    /**
     *  url地址:/strType/status/{id}/{status}
     *  参数: 利用strType对象接收
     *  返回值: SysResult对象
     */
    @PutMapping("/status/{id}/{status}")
    public SysResult updateStatus(StrType strType){

        strTypeService.updateStatus(strType);
        return SysResult.success();
    }

    /**
     * URL:  /strType/saveStrType
     * 参数:  form表单提交 json
     * 返回值: SysResult对象
     */
    @PostMapping("/saveStrType")
    public SysResult saveItemCat(@RequestBody StrType strType){
        //状态 赋默认值
        strTypeService.saveStrType(strType);
        return SysResult.success();
    }

}
