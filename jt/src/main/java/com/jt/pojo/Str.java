package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("str")
public class Str extends BasePojo {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer userId;
    private String  title;
    private String  strDesc;
    private String images;
    private Integer strTypeId;
    private Integer cityId;
    private boolean status;
}
