package com.jt.service;

import com.jt.pojo.User;
import com.jt.vo.PageResult;

import java.util.List;

public interface UserService {

    String login(User user);


    List<User> findAll();

    PageResult getUserList(PageResult pageResult);

    void updateStatus(User user);

    void addUser(User user);

    void updateUser(User user);

    User getUser(Integer id);

    void deleteUser(Integer id);
}
