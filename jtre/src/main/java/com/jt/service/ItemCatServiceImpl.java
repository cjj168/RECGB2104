package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.ItemCatMapper;
import com.jt.pojo.ItemCat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ItemCatServiceImpl implements ItemCatService{

    @Autowired
    private ItemCatMapper itemCatMapper;



    /**
     * 业务: 查询3级商品分类信息
     *      1. 一级中嵌套二级集合
     *      2. 二级菜单嵌套三级集合.
     *
     * 1.0版本: for循环嵌套结构  暂时不考虑level 最好理解的
     * 常识:
     *      1.用户第一次查询数据库 需要建立链接.
     *      2.第二次查询 从链接池中动态获取链接 所以速度更快!!!
     *
     *  思考: 该业务查询了多少次数据库??? 第一层循环10个  第二层循环10 总查询数=10*10=100次
     *       如何优化查询策略!!!!
     * @param
     * @return
     */


    //方式一:查询商品分类信息
//    @Override
//    public List<ItemCat> findItemCatList(Integer level) {
//        //性能测试
//        long startTime = System.currentTimeMillis();
//        //获取一级商品
//        QueryWrapper queryWrapper = new QueryWrapper();
//        queryWrapper.eq("parent_id", 0);
//        List<ItemCat> oneList = itemCatMapper.selectList(queryWrapper);
//        //获取二级商品信息,需要遍历一级商品信息
//        for (ItemCat oneItemCat : oneList) {
//            queryWrapper.clear();
//            queryWrapper.eq("parent_id", oneItemCat.getId());
//            List<ItemCat> towList = itemCatMapper.selectList(queryWrapper);
//            //获取三级商品信息,需要遍历二级商品信息
//            for (ItemCat towItemCat : towList) {
//                queryWrapper.clear();
//                queryWrapper.eq("parent_id", towItemCat.getId());
//                List<ItemCat> threeList = itemCatMapper.selectList(queryWrapper);
//                //将三级商品信息封装到二级商品信息中
//                towItemCat.setChildren(threeList);
//            }
//            //将二级商品信息封装到一级商品信息中
//            oneItemCat.setChildren(towList);
//        }
//        long endTime = System.currentTimeMillis();
//        System.out.println("分类查询时间为"+(endTime-startTime)+"毫秒");
//        return oneList;
//    }

    public Map<Integer,List<ItemCat>> itemCatMap(){
        Map<Integer,List<ItemCat>> map = new HashMap<>();
        List<ItemCat> itemCatList = itemCatMapper.selectList(null);
        for (ItemCat itemCat : itemCatList) {
            if (map.containsKey(itemCat.getParentId())){
                List<ItemCat> exists = map.get(itemCat.getParentId());
                exists.add(itemCat);
            }else {
                List<ItemCat> firstList =new ArrayList<>();
                firstList.add(itemCat);
                map.put(itemCat.getParentId(), firstList);
            }

        }
        return map;
    }


    @Override
    public List<ItemCat> findItemCatList(Integer level) {
        long startTime = System.currentTimeMillis();
        Map<Integer, List<ItemCat>> map = itemCatMap();
        if (level==1){
            return map.get(0);
        }
        if (level==2){
            return getLevel2List(map);
        }
        List<ItemCat> oneList = getLevel2List(map);
        for (ItemCat itemCat : oneList) {
            List<ItemCat> towList = itemCat.getChildren();
            if (towList==null||towList.size()==0){
                continue;
            }
            for (ItemCat towItemCat : towList) {
                List<ItemCat> threeList = map.get(towItemCat.getId());
                towItemCat.setChildren(threeList);
            }
        }
        long endTime = System.currentTimeMillis();
        System.out.println("分类查询时间为"+(endTime-startTime)+"毫秒");
        return oneList;
    }

    private List<ItemCat> getLevel2List(Map<Integer, List<ItemCat>> map) {
        List<ItemCat> oneList = map.get(0);
        for (ItemCat itemCat : oneList) {
            List<ItemCat> towList = map.get(itemCat.getId());
            itemCat.setChildren(towList);
        }
        return oneList;
    }
    //商品状态修改
    @Override
    @Transactional
    public void updateStatus(ItemCat itemCat) {
        itemCatMapper.updateById(itemCat);
    }

    //商品新增
    @Override
    @Transactional
    public void saveItemCat(ItemCat itemCat) {
        itemCat.setStatus(true);
        itemCatMapper.insert(itemCat);
    }
    //商品分类的删除
    @Override
    public void deleteItemCat(ItemCat itemCat) {
        if (itemCat.getLevel() == 3){
            itemCatMapper.deleteById(itemCat.getId());
            return;
        }
        if (itemCat.getLevel() ==2){
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("parent_id", itemCat.getId());
            itemCatMapper.delete(queryWrapper);
            itemCatMapper.deleteById(itemCat.getId());
            return;
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("parent_id", itemCat.getId());
        List<ItemCat> towList = itemCatMapper.selectList(queryWrapper);
        for (ItemCat towItemCat : towList) {
            queryWrapper.clear();
            queryWrapper.eq("parent_id", towItemCat.getId());
            itemCatMapper.delete(queryWrapper);
            itemCatMapper.deleteById(towItemCat.getId());
        }
        itemCatMapper.deleteById(itemCat.getId());
    }

    //商品更新
    @Override
    public void updateItemCat(ItemCat itemCat) {
        itemCatMapper.updateById(itemCat);
    }

}
