package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.ItemDescMapper;
import com.jt.mapper.ItemMapper;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.vo.ItemVO;
import com.jt.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.beans.Transient;
import java.util.List;

@Service
 public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemMapper itemMapper;
    @Autowired
    private ItemDescMapper itemDescMapper;

    /**
     * select * from item limit (n-1)m,m
     * @param pageResult
     * @return
     */
//    @Override
//    public PageResult getItemList(PageResult pageResult) {
//        long total = itemMapper.selectCount(null);
//        int pageNum = pageResult.getPageNum();
//        int pageSize = pageResult.getPageSize();
//        int startIndex = (pageNum-1)*pageSize;
//        List<Item> rows = itemMapper.getItemList(pageResult);
//        return pageResult.setTotal(total).setRows(rows);
//    }

    /**
     * 1.手写Sql 一种写法
     * 2.利用MP机制实现分页
     * @param pageResult
     * @return
     */
    @Override
    public PageResult getItemList(PageResult pageResult) {//3个
        //创建分页对象
        IPage iPage = new Page(pageResult.getPageNum(),pageResult.getPageSize());
        //
        QueryWrapper queryWrapper = new QueryWrapper();
        boolean flag = StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag,"title",pageResult.getQuery());
        //执行分页查询,返回值依然是分页对象信息
        iPage = itemMapper.selectPage(iPage, queryWrapper);
        //回传5个参数 total/分页后的数据
        long total = iPage.getTotal();
        List<Item> rows = iPage.getRecords();
        return pageResult.setTotal(total).setRows(rows);
    }

    @Override
    @Transactional
    public void saveItem(ItemVO itemVO) {
        Item item = itemVO.getItem();
        item.setStatus(true); //默认启用状态
        itemMapper.insert(item);//MP自动的实现了主键自动回显
        ItemDesc itemDesc = itemVO.getItemDesc();
        itemDesc.setId(item.getId());//MP回显之后取值操作
        itemDescMapper.insert(itemDesc);
    }

    @Override
    @Transactional
    public void deleteItemById(Item item) {
        itemMapper.deleteById(item.getId());
        itemDescMapper.deleteById(item.getId());
    }

    @Override
    @Transactional
    public void updateItemStatus(Item item) {
        itemMapper.updateById(item);
    }


}
