package com.jt.service;

import com.jt.vo.ImageVO;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;


@Service
public class FileServiceImpl implements FileService{
    private String localDirPath = "D:/images";
    /**
     * 1.验证上传的文件是图片!  jpg|png|gif  采用正则的方式校验
     * 2.防止恶意程序攻击,  验证图片是否有宽度和高度.
     * 3.文件分目录存储     例如: /2021/11/11
     *                    例如2: hash码 8位hash xx/xx/xx/xx
     *  数据hahs时 特征:因为是算法 可能造成数据分配不均!!!
     * 4.防止文件重名,修改文件名称 UUID
     * @param file
     * @return
     */
    @Override
    public ImageVO upload(MultipartFile file) {
        //1.校验图片类型是否正确   正则表达式 a.jpg
        //1.1 获取文件名称
        String filename = file.getOriginalFilename();
        filename= filename.toLowerCase();
        //1.3将名称全部小写
        if (!filename.matches("^.+\\.(jpg|png|gif)$")){
            //如果文件不是图片 则返回null
             return null;
        }
        //2.判断是否为恶意程序
        //2.1 通过图片对象 获取宽度和高度
        try {
            BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
            int height = bufferedImage.getHeight();
            int width = bufferedImage.getWidth();
            if (height==0||width==0){
                return null;
            }
            //3实现分目录存储
            //3.1 按照时间将分配目录  /yyyy/MM/dd/
            String dateDirPath = new SimpleDateFormat("/yyyy/MM/dd/").format(new Date());
            //3.2 "D:/images/yyyy/MM/dd/
            String localDir = localDirPath + dateDirPath;
            File dirFile = new File(localDir);
            if (!dirFile.exists()){
                dirFile.mkdirs();
            }
            //4. 防止文件重名  生成UUID.文件类型
            String uuid = UUID.randomUUID().toString().replace("-", "");
            int index = filename.lastIndexOf(".");
            //获取后缀.jpg
            String fileType = filename.substring(index);
            String realFileName = uuid + fileType;

            //5.实现文件上传
            // 5.1 拼接文件全路径 目录/文件名称
            String localFilePath = localDir + realFileName;
            // 5.2 完成文件上传
            file.transferTo(new File(localFilePath));

            /**封装返回值.
             * 不带磁盘的本地存储路径 虚拟动态目录
             * D:\images\2021\07\13\8b7b3ef3c88845a6876371b4815244ca.jpg
             */
            String virtualPath = dateDirPath + realFileName;
            ImageVO imageVO = new ImageVO(virtualPath,"",realFileName);
            return imageVO;
        } catch (IOException e) {
            e.printStackTrace();
            //终止程序
            return null;
        }

    }
//    @Override
//    public void upload(MultipartFile file) {
//        /**
//         * 步骤:
//         *     1.准备文件上传的目录
//         *     2.获取文件上传名称
//         *     3.拼接文件路径
//         *     4.实现文件上传
//         *  关于业务层异常处理原则: 将检查异常转化为运行时异常
//         * @param file
//         */
//        String filePath = "D:/images/";
//        File fileDri = new File(filePath);
//        if (!fileDri.exists()){
//            fileDri.mkdirs();
//        }
//        String filename = file.getOriginalFilename();
//        String path = filePath + filename;
//        try {
//            file.transferTo(new File(path));
//        } catch (IOException e) {
//            e.printStackTrace();
//            throw new RuntimeException(e);
//        }
//    }
}
