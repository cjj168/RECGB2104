package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import com.jt.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;

    @Override
    public String login(User user) {
        //1.获取原始密码
        String password = user.getPassword();
        //2.将密码进行加密处理
        String md5Str = DigestUtils.md5DigestAsHex(password.getBytes());
        //3.将密文传递给对象
        user.setPassword(md5Str); //username/password
        //根据对象中不为null的属性充当where条件  username="xx" and password="xxx"
        QueryWrapper<User> queryWrapper = new QueryWrapper(user);
        //4.根据条件查询数据库
        User userDB = userMapper.selectOne(queryWrapper);
        //5.定义token数据 限定条件 token不能重复
        // UUID: 随机字符串 3.4*10^38 种可能性  hash(时间戳+随机数)
        String uuid = UUID.randomUUID().toString()
                .replace("-","");
        return userDB==null?null:uuid;

    }

    @Override
    public List<User> findAll() {
        return userMapper.selectList(null);
    }




//    @Override
//    public PageResult getUserList(PageResult pageResult) {
//        //1.获取总记录数
//        long total = userMapper.selectCount(null);
//        //2.获取分页结果
//        //2.1获取页数
//        int pageNum = pageResult.getPageNum();
//        //2.2获取条数
//        int pageSize = pageResult.getPageSize();
//        int startIndex = (pageNum-1)*pageSize;
//        //List<User> rows = userMapper.getUserList(startIndex,pageSize);
//        List<User> rows = userMapper.getUserList2(pageResult);
//
//        //将数据封装到pageResult对象中
//        pageResult.setTotal(total).setRows(rows);
//        return pageResult;
//
//    }

    /**
     * api说明:selectPage(arg1,arg2)
     * arg1:分页对象  arg2:条件构造器
     *
     * IPage(arg1,arg2) 分页对象
     * arg1:页数   arg2:行数
     * @param pageResult
     * @return
     */
    @Override
    public PageResult getUserList(PageResult pageResult) {
        IPage iPage = new Page(pageResult.getPageNum(),pageResult.getPageSize());
        QueryWrapper queryWrapper = new QueryWrapper();
        boolean flag = StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag,"username",pageResult.getQuery());
        //将获取到的数据装成分页对象
        iPage = userMapper.selectPage(iPage, queryWrapper);
        long total = iPage.getTotal();
        System.out.println(total);
        List<User> rows = iPage.getRecords();
        return pageResult.setTotal(total).setRows(rows);
    }

    @Override
    @Transactional
    public void updateStatus(User user) {
        userMapper.updateById(user);
    }

    @Override
    @Transactional
    public void addUser(User user) {
        //将明文加密
        String password = user.getPassword();
        String md5Str = DigestUtils.md5DigestAsHex(password.getBytes());
        //将密文传给对象
        user.setPassword(md5Str).setStatus(true);
        userMapper.insert(user);
    }


    @Override
    public User getUser(Integer id) {
        return userMapper.selectById(id);
    }
    @Override
    @Transactional
    public void updateUser(User user) {
        userMapper.updateById(user);
    }
    @Override
    @Transactional
    public void deleteUser(Integer id) {
        userMapper.deleteById(id);
    }

}
