package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.RightsMapper;
import com.jt.pojo.Rights;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RightsServiceImpl implements RightsService{

    @Autowired
    private RightsMapper rightsMapper;

    /**
     * 1.查询所有的1级信息 parent_id=0
     * 2.查询1级下边的2级信息 parent_id=1级id
     * @return
     */
    @Override
    public List<Rights> getRightsList() {
        //1.查询一级权限信息
        QueryWrapper<Rights> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_Id", 0);
        List<Rights> rightsList = rightsMapper.selectList(queryWrapper);
        //2.查询1级下边的二级list集合
        for (Rights rights : rightsList){
            queryWrapper.clear();//将where条件清空之后重新添加
            queryWrapper.eq("parent_Id", rights.getId());
            List<Rights> towList = rightsMapper.selectList(queryWrapper);
            rights.setChildren(towList);
        }

        return rightsList;
    }
}
