package com.jt.service;

import com.jt.pojo.Item;
import com.jt.vo.ItemVO;
import com.jt.vo.PageResult;

public interface ItemService {
    PageResult getItemList(PageResult pageResult);

    void saveItem(ItemVO itemVO);

    void deleteItemById(Item item);

    void updateItemStatus(Item item);
}
