package com.jt.controller;

import com.jt.pojo.ItemCat;
import com.jt.service.ItemCatService;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/itemCat")
public class ItemCatController {
    @Autowired
    private ItemCatService itemCatService;
    /**
     * 业务需求:查询商品分类信息
     * url:/itemCat/findItemCatList/{level}
     * 请求类型:get
     * 请求参数:level
     * 返回值:SysResult对象(封装商品分类信息)
     */
    @GetMapping("/findItemCatList/{level}")
    public SysResult findItemCatList(@PathVariable Integer level){
        List<ItemCat> itemCatList = itemCatService.findItemCatList(level);
        return SysResult.success(itemCatList);
    }

    /**
     * 业务需求:商品状态修改
     * url:/itemCat/status/{id}/{status}
     * 请求类型:put
     * 请求参数:{id}/{status}(利用ItemCat对象接收)
     * 返回值:SysResult对象
     */
    @PutMapping("/status/{id}/{status}")
    public SysResult updateStatus(ItemCat itemCat){
        itemCatService.updateStatus(itemCat);
        return SysResult.success();
    }

    /**
     * 业务需求:商品新增
     * url:/itemCat/saveItemCat
     * 请求类型:post
     * 请求参数:form表单
     * 返回值:SysResult对象
     */
    @PostMapping("/saveItemCat")
    public SysResult saveItemCat(@RequestBody ItemCat itemCat){
        itemCatService.saveItemCat(itemCat);
        return SysResult.success();
    }
    /**
     * 业务需求:商品分类删除
     * url:/itemCat/deleteItemCat
     * 请求类型:delete
     * 请求参数:ItemCat(对象)
     * 返回值:SysResult对象
     */
    @DeleteMapping("/deleteItemCat")
    public SysResult deleteItemCat(ItemCat itemCat){
        itemCatService.deleteItemCat(itemCat);
        return SysResult.success();
    }

    /**
     * 业务需求:商品分类更新
     * url:/itemCat/updateItemCat
     * 请求类型:put
     * 请求参数:ItemCat(对象)
     * 返回值:SysResult对象
     */
    @PutMapping("/updateItemCat")
    public SysResult updateItemCat(@RequestBody ItemCat itemCat){
        itemCatService.updateItemCat(itemCat);
        return SysResult.success();
    }

}
