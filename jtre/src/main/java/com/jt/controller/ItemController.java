package com.jt.controller;

import com.jt.pojo.Item;
import com.jt.service.ItemService;
import com.jt.vo.ItemVO;
import com.jt.vo.PageResult;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/item")
public class ItemController {
    @Autowired
    private ItemService itemService;
    /**
     * 业务需求:商品列表展现
     * url:/item/getItemList?query=&pageNum=1&pageSize=10
     * 请求参数:pageResult
     * 请求方式:get
     * 返回类型:SysResult(封装pageResult对象)
     */
    @GetMapping("/getItemList")
    public SysResult getItemList(PageResult pageResult){
        pageResult = itemService.getItemList(pageResult);
        return SysResult.success(pageResult);

    }
    /**
     * URL地址:  http://localhost:8091/item/saveItem
     * 请求参数:  form表单的提交对象~~~JSON串 ItemVO对象接收
     * 返回值:   SysResult对象
     */
    @PostMapping("/saveItem")
    public SysResult saveItem(@RequestBody ItemVO itemVO){
        itemService.saveItem(itemVO);
        return SysResult.success();
    }
    /**
     * URL地址:  http://localhost:8091/item/deleteItemById
     * 请求参数:  item对象
     * 返回值:   SysResult对象
     */
    @DeleteMapping("/deleteItemById")
    public SysResult deleteItemById(Item item){
        itemService.deleteItemById(item);
        return SysResult.success();
    }
    /**
     * 业务需求:商品状态修改
     * url:/item/updateItemStatus
     * 请求类型:put
     * 请求参数:(利用ItemCat对象接收)
     * 返回值:SysResult对象
     */
    @PutMapping("/updateItemStatus")
    public SysResult updateItemStatus(@RequestBody Item item){
        itemService.updateItemStatus(item);
        return SysResult.success();
    }
    /**
     * 业务需求:商品状态修改
     * url:/item/updateItemStatus
     * 请求类型:put
     * 请求参数:(利用ItemCat对象接收)
     * 返回值:SysResult对象
     */


}
