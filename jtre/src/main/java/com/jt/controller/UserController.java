package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import com.jt.vo.PageResult;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/findAll")
    public List<User> findAll(){
        return userService.findAll();

    }


    @PostMapping("/login")
    public SysResult login(@RequestBody User user){
        String token = userService.login(user);
        if (StringUtils.hasLength(token)){
            return SysResult.success(token);
        }
        return SysResult.fail();
    }
    /**
     * 业务需求:用户列表展现
     * url:
     * 请求参数
     * 请求方式
     * 返回类型
     */
    @GetMapping("/list")
    public SysResult getUserList(PageResult pageResult){
        pageResult = userService.getUserList(pageResult);
        return SysResult.success(pageResult);
    }
    /**
     * 业务需求:用户状态修改
     * url:/user/status/{id}/{status}
     * 请求参数:{id}/{status}
     * 请求方式:put
     * 返回类型:SysResult对象
     */
    @PutMapping("/status/{id}/{status}")
    public SysResult updateStatus(User user){
        userService.updateStatus(user);
        return SysResult.success();
    }

    /**
     * 业务需求:用户新增
     * url:/user/addUser
     * 请求参数:form表单
     * 请求方式:post
     * 返回类型:SysResult对象
     */
    @PostMapping("/addUser")
    public SysResult addUser(@RequestBody User user){
        userService.addUser(user);
        return SysResult.success();
    }
    /**
     * 业务需求:通过id查询用户
     * url:/user/{id}
     * 请求参数:id
     * 请求方式:get
     * 返回类型:SysResult对象(携带user对象)
     */
    @GetMapping("/{id}")
    public SysResult getUser(@PathVariable Integer id){
        User user = userService.getUser(id);
        return SysResult.success(user);

    }

    /**
     * 业务需求:用户信息修改
     * url:/user/updateUser
     * 请求参数:user对象
     * 请求方式:put
     * 返回类型:SysResult对象
     */
    @PutMapping("/updateUser")
    public SysResult updateUser(@RequestBody User user){
        userService.updateUser(user);
        return SysResult.success();
    }

    /**
     * 业务需求: 删除用户
     * url:/user/{id}
     * 请求参数:id
     * 请求方式:delete
     * 返回类型:SysResult对象
     */
    @DeleteMapping("/{id}")
    public SysResult deleteUser(@PathVariable Integer id){
        userService.deleteUser(id);
        return SysResult.success();
    }

}
